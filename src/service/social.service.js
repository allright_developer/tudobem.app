import * as Facebook from "fb-sdk-wrapper";

export default {
  onFacebook,
  getWhatsappUrl,
  getUserAffiliateLink,
  init
}

const props = {
  quote: 'Oi, tudobem? Quero te convidar para fazer parte do Bem! O *tudobem* é uma App de Cashback Social. A cada ação sua no app _tudobem_, você ganha *Credibens*' +
    ' para você e para Instituições Beneficentes, é Incrível! Faça o seu Cadastro Gratuito usando o meu Link e o app dará *Credibens* para mim, para Você e ' +
    'para uma das Instituições Beneficentes apoiadas pelo App *tudobem*. ',
  quoteFacebook: 'Oi, 𝙩𝙪𝙙𝙤𝙗𝙚𝙢? Quero te convidar para fazer parte do Bem! A cada ação sua no aplicativo 𝙩𝙪𝙙𝙤𝙗𝙚𝙢, você ganha 𝘾𝙧𝙚𝙙𝙞𝙗𝙚𝙣𝙨' +
    ' para você e para Instituições Beneficentes, é Incrível! ' +
    'Faça o seu Cadastro Gratuito usando o meu Link: O app 𝙩𝙪𝙙𝙤𝙗𝙚𝙢 dará 𝘾𝙧𝙚𝙙𝙞𝙗𝙚𝙣𝙨 para Você e ' +
    'para uma das Instituições Beneficentes apoiadas pela Iniciativa. ',

  href: 'https://tudobem.app/',
  hashtag: '#tudobem #doeganhe #socialcashback #produtosgrátis #correntedobem #promocao #produtosgratis'
};

/**
 * Shares on Facebook
 * @param {Object} params:
 * href: the url to share,
 * params: params in Object format,
 * quote: text to share along with URL, hashtag: hashtag strings
 * @returns {*}
 */
function onFacebook( params )
{
  let href = params.href || props.href;
  if( params.params )
    href += getUrlParams( params.params, href );

  return Facebook.ui({ method: 'share', quote: params.quote || props.quoteFacebook, hashtag: params.hashtag || props.hashtag, href: href });
}

function init()
{
  Facebook.load()
  .then(() => {
    Facebook.init({
      appId: '2496718463876192'
    });
    initTracker();
  });
}

function initTracker()
{
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script', 'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '951615018584260');
  fbq('track', 'PageView');
}
/**
 *
 * @param {Object} params
 * env['api'|'web']: defined according to the user device, <br>
 * <b>quote</b>: the text to share along with URL, <br>
 * <b>href</b>: a custom link to share, <br>
 * <b>params</b>: object params to be converted to url params
 * @returns {string}
 */
function getWhatsappUrl( params )
{
  let env = params.env || 'api';
  let href = params.href || props.href;
  if( params.params )
    href += getUrlParams( params.params, href );

  // let encodedText = window.encodeURIComponent( params.quote || props.quote );
  let encodedText = params.quote || props.quote;
  let url = `https://${env}.whatsapp.com/send?text=`+encodedText+href;
  window.open( url, '_blank' );
}

/**
 * Builds and returns the shareable affiliate link
 * @param {Object} params
 * @returns {string}
 */
function getUserAffiliateLink( params )
{
  return props.href+getUrlParams( params, props.href );
}
/**
 * Converts a key:value Object in stringified url params
 * @param {Object} params url params in object format
 * @param {String} link
 * @returns {string}
 */
function getUrlParams( params, link )
{
  let p = link.indexOf( '?' ) === -1 ? '?' : '';

  if( Object.keys( params ).length )
    p += Object.keys( params ).map(key => key + '=' + params[key]).join('&');

  return p;
}
