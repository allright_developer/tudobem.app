// Configuration for your app
// https://quasar.dev/quasar-cli/quasar-conf-js
const fs = require('fs')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = function (ctx) {
  return {
    // app boot file (/src/boot)
    // --> boot files are part of "main.js"
    boot: [
      'main.js'
    ],

    css: [
      'app.styl'
    ],
    vendor:{
      remove:[ 'axios', 'unfetch' ]
    },
    extras: [
      // 'ionicons-v4',
      // 'mdi-v3',
      'fontawesome-v5',
      // 'eva-icons',
      // 'themify',
      // 'roboto-font-latin-ext', // this or either 'roboto-font', NEVER both!

      // 'roboto-font', // optional, you are not bound to it
      // 'material-icons' // optional, you are not bound to it
    ],

    framework: {
      iconSet: 'fontawesome-v5',
      lang: 'pt-br', // Quasar language

      all: 'auto', // --- includes everything; for dev only!

      components: [],

      directives: [],

      // Quasar plugins
      plugins: [
        'Notify'
      ]
    },

    build: {
      chainWebpack (chain) {
        /*if (ctx.mode.pwa) {
          chain
            .plugin('copy-static-files')
            .use(require('copy-webpack-plugin'), [
              [{
                from: 'src-pwa/firebase-messaging-sw.js',
                to: '',
              }]
            ])
        }*/
      },
      scopeHoisting: true,
      // modern: true, // modern ES6+ build
      vueRouterMode: 'history',
      minify: true,
      // vueCompiler: true,
      gzip: true,
      // analyze: true,
      // extractCSS: false,
      env:{
        fetchBaseURL: ctx.dev ? '/api/' : 'https://staging.api.tudobem.app/api/',
        fetchDefaults: {
          fetchOptions: {
            method:'POST',
            headers: {
              'Content-type':'application/json'
            }
          }
        }
      },
      extendWebpack (cfg) {
        cfg.plugins.push(
          new CopyWebpackPlugin({
            patterns:[
              {
                from: 'src-pwa/firebase-messaging-sw.js', to: ''
              }
            ]
          })
        );
      }
    },

    devServer: {
      /*https: {
        key: fs.readFileSync('/var/www/public/app/dev.tudobem.app/localhost+2-key.pem'),
        cert: fs.readFileSync('/var/www/public/app/dev.tudobem.app/localhost+2.pem'),
        ca: fs.readFileSync('/var/www/public/app/dev.tudobem.app/rootCA.pem' )
      },*/
      // host: 'dev.tudobem',
      // port: 8080,
      open: false, // opens browser window automatically
      proxy:{
        '/api':{
          target:'http://api.local.tudobem/api',
          changeOrigin:true,
          pathRewrite:{
            '^/api' : ''
          }
        }
      }
    },

    // animations: 'all', // --- includes all animations
    animations: [
      // 'fadeInUp', 'fadeOutUp','bounce'
    ],

    ssr: {
      pwa: false
    },

    pwa: {
      // workboxPluginMode: 'InjectManifest',
      // workboxOptions: {}, // only for NON InjectManifest
      manifest: {
        name: 'tudobem',
        short_name: 'tudobem',
        description: 'tudobem: Você ganha e quem precisa também',
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#027be3',
        icons: [
          {
            'src': 'icons/icon-128x128.png',
            'sizes': '128x128',
            'type': 'image/png'
          },
          {
            'src': 'icons/icon-192x192.png',
            'sizes': '192x192',
            'type': 'image/png'
          },
          {
            'src': 'icons/icon-256x256.png',
            'sizes': '256x256',
            'type': 'image/png'
          },
          {
            'src': 'icons/icon-384x384.png',
            'sizes': '384x384',
            'type': 'image/png'
          },
          {
            'src': 'icons/icon-512x512.png',
            'sizes': '512x512',
            'type': 'image/png'
          }
        ]
      }
    },

    cordova: {
      // id: 'org.cordova.quasar.app',
      // noIosLegacyBuildFlag: true, // uncomment only if you know what you are doing
    },

    electron: {
      // bundler: 'builder', // or 'packager'

      extendWebpack (cfg) {
        // do something with Electron main process Webpack cfg
        // chainWebpack also available besides this extendWebpack
      },

      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options

        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',

        // Windows only
        // win32metadata: { ... }
      },

      builder: {
        // https://www.electron.build/configuration/configuration

        appId: 'tudobem-app'
      }
    }
  }
}
