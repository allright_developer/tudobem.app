/**
 * get the current timestamp
 * @returns {number}
 */
export function getTs()
{
  return Math.floor(Date.now() / 1000);
}

/**
 * get a timestamp in a future point
 * @param {number} val the value
 * @param {String} period the letter representation: m, h, d
 * @returns {Number} the timestamp in a future point
 */
export function getExpiredAtTimestamp( val, period )
{
  period = period || 'm';
  let ts = 0;
  switch ( period ){
    case 'm':
      ts = val * 60;
      break;
    case 'h':
      ts = val * (60*60);
    case 'd':
      ts = val * (24*60*60);
      break;
    default:
      ts = val;
      break;
  }
  return getTs() + ts; // sums current timestamp with additional time in seconds
}
