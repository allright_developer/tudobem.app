/**
 * Use it as a mixin into the Forms
 */
export default {

  data() {
    return {
      messages: {
        'phone_not_related': 'O telefone informado não está associado ao seu Cadastro.',
        'login_error': 'O Email informado não está associado a uma Conta. Por favor, verifique os dados informados.'
      }
    }
  },
  methods: {
    email_remote(val) {
      this.form.loading = true;
      return new Promise((resolve, reject) => {

        // mocking up remote validation
        setTimeout(() => {
          let res = val !== null && val.length > 0 || '* Por favor, informe um email válido';

          this.form.loading = false;
          resolve(res);
        }, 1500);

      });
    },
    email_format(val) {
      val = val.replace( /\s+/g, '' );
      let res = val && /(^$|^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$)/.test(val)
        || 'Por favor, informe um Email válido';

      if (res === true) {
        let tld = val.split('@')[1];
        let grantedTLD = /gmail.com$|hotmail.com$|hotmail.com.br$|outlook.com$|globomail.com$|yahoo.com$|yahoo.com.br$|oi.com.br$|globo.com$|bol.com.br$|uol.com.br$|live.com$|ymail.com$|facebook.com$|r7.com$|ibest.com.br$|me.com$|terra.com.br$|ig.com.br$|zipmail.com.br$|msn.com$/;
        res = grantedTLD.test(tld);
      }

      return res || 'Por favor, informe um domínio de um serviço de Email válido';
    },
    name_format(val) {
      return val.length > 4 || 'Por favor, informe o seu Nome Completo';
    },
    phone_format(val) {
      val = val.replace(/\D/g, '');
      let nine = val.substr(2, 1);
      return (val.length === 11 && nine === '9') || 'Por favor, informe um Celular com DDD válido';
    }
  }
};
