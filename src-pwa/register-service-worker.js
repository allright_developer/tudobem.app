import {register} from 'register-service-worker'
import { initMessaging, state } from "src/service/firebase/fcm.service";
import { Notify } from "quasar";
// The ready(), registered(), cached(), updatefound() and updated()
// events passes a ServiceWorkerRegistration instance in their arguments.
// ServiceWorkerRegistration: https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration
register(process.env.SERVICE_WORKER_FILE, {
// register('firebase-messaging-sw.js', {
  // The registrationOptions object will be passed as the second argument
  // to ServiceWorkerContainer.register()
  // https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerContainer/register#Parameter

  // registrationOptions: { scope: './' },

  /**
   *
   * @param {ServiceWorkerRegistration} registration
   */
  ready(registration){},

  registered( registration )
  {
    state.registration = registration;
    if ( 'Notification' in window ) {
      initMessaging();
      // state.messaging.useServiceWorker( registration );
      if ( Notification.permission === "default" ){

        // @todo move to how-to onboard page
        /*setTimeout(() => {
          let n = Notify.create({
            message: 'Receba Notificações de Novos <b>Produtos para Testar</b>, <b> Pesquisas e Campanhas</b> do <b class="text-secondary">tudobem</b>!',
            timeout: 8000, progress: true, html: true, multiLine: true,
            icon: 'fas fa-comment-dollar', color: 'white', textColor: 'primary', position: 'center', /!*icon: 'fas fa-heart',*!/
            actions:[
              // { label: 'depois', color: 'negative', handler: () => { n() }, icon: 'fas fa-history' },
              {
                label: 'Receber Notificações', color: 'positive', handler: () => { requestPushPermission(); }
                ,icon: 'far fa-bell'
              }
            ]
          });

        }, 1000 );*/

        // new push messages listener
        state.messaging.onMessage((payload) => {
          Notify.create({
            message: payload.notification.body, timeout: 7000, progress: true,
            avatar: 'icons/apple-icon-120x120.png', color: 'primary', textColor: 'white', position: 'center', /*icon: 'fas fa-heart',*/
            actions:[{
              label: 'Saiba mais', color: 'white', handler: () => { location.href = payload.notification.click_action }
            }]
          });
        });

        /*state.messaging.setBackgroundMessageHandler(async payload => {
          // Customize notification here
          console.log('BACKGROUND MESSAGE PAYLOAD', payload);
          const notificationTitle = 'Título da Mensagem';
          const notificationOptions = {
            body: 'Mensagem a ser aplicada no body do dialog',
            icon: 'icons/apple-icon-120x120.png',
            badge: 'icons/apple-icon-120x120.png',
            image: 'img/tudobem-logo-fonte-512.png'
          };
          return registration.showNotification( notificationTitle, notificationOptions );
        });*/
        // token refresh listener
        state.messaging.onTokenRefresh( () => {
          state.messaging.getToken().then( token => {
            localStorage.setItem( 'ptkn', token );
          })
          .catch( error => console.log( 'error on get token', error ) );
        });
      }
    }
  },

  cached(registration) {},

  updatefound(/* registration */) {},

  updated(/* registration */) {},

  offline() {},

  error( err ) {
    console.error('An Error occurred :', err)
  }
})
