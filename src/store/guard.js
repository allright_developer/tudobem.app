import GuardService from 'src/service/guard.service';

let initialState = { authenticated: false, user: null, errors:null, ts: 0 };
try{
  const yzr = GuardService.isAuthenticated();
  initialState = yzr ?
    { authenticated: true, user: yzr.data.user, user_data: yzr.data.user_data, credits: yzr.data.credits, errors: null, ts: yzr.ts } :
    { authenticated: false, user: null, credits: null, errors:null, ts:0 };

}
catch( e ){
  console.log( e );
}

export default {
  namespaced: true,
  state: initialState,

  mutations:{

    login( state, json )
    {
      if( json ){
        state.authenticated = true;
        state.user = json.data.user;
        state.user_data = json.data.user_data;
        state.credits = json.data.credits;
        state.ts = GuardService.getTimestamp();
      }
    }

    ,logout( state )
    {
      state.authenticated = false;
      state.user = null;
      state.credits = null;
      GuardService.removeStorage();
    }

    ,addCredits( state, amount)
    {
      if( state.credits !== null )
          state.credits.crypto += amount;
    }

    ,setCredits( state, credits )
    {
      state.credits = credits;
    }

    ,user_data( state, data )
    {
      state.user_data = data;
    }

    ,setErrors( state, errors )
    {
      state.errors = errors;
    }
  },

  actions:
  {
    async login({ commit }, { email, password } ){
       await GuardService.login({email, password}).then( json => {

          if( json && json.success ){
            GuardService.setStorage( json );
            commit( 'login', json );
            // commit( 'setCredits', json );
            commit( 'setErrors', null );
          }
          else{
            commit('setErrors', json.error );
          }
       })
      .catch( error => {
        console.error('login error', error );
      });
    }

    ,async register({commit}, {name, email, phone, query }){
      let ptkn = localStorage.getItem( 'ptkn' );
      await GuardService.register({name, email, phone, query, ptkn }).then(json => {

        if( json !== null && json.success ){
          GuardService.setStorage( json );
          localStorage.setItem( 'hasAccount', 'true' );
          commit( 'login', json );
          commit( 'setErrors', null );
        }
        else{
          // commit('setErrors', json.messages );
          commit('setErrors', { 'manutencao': 'Pedimos desculpas, mas nosso site está em manutenção. Agradecemos a sua Compreensão.'} );
        }
      });
    }

    ,logout({commit})
    {
      commit('logout');
    }

    ,async addCredits({commit}, amount )
    {
      await GuardService.addCredits({ crypto:amount }).then( json => {

        if( json.success ){
          commit('setCredits', json.data );
        }
        else{
          commit('setErrors', json.messages );
        }
      });

    }

    ,setCredits({commit}, credits)
    {
        let yzr = GuardService.isAuthenticated();
        yzr.data.credits = credits;
        GuardService.setStorage( yzr );
        commit('setCredits', credits );
    },

    setErrors({commit}, errors )
    {
      commit( 'setErrors', errors );
    },

    isAuthenticated({commit})
    {
      return GuardService.isAuthenticated();
    },

    hasAccount()
    {
      return JSON.parse( localStorage.getItem( 'hasAccount' ) );
    },

    async refreshState({ commit, state })
    {
      let ts = Math.floor(Date.now() / 1000);
      let res = true;
      // console.log('refreshing state?', state.ts < ts, state.ts, ts );
      if( state.ts < ts ) {
        GuardService.removeStorage();
        res = false;
      }
      return res;
    },

    async update( {commit}, data )
    {
      commit( 'user_data', data );
      return await GuardService.update( data );
    }
  },

  getters:{
    getErrors: state => {
        return state.errors;
    }
    ,getTimestamp: state => { return state.ts; }
  }

}
