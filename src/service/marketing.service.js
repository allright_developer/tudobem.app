export const copies = {
  'maquiagem': {
    id_category: 11,
    assets: {
      header: 'produtos_tudobem_maquiagem.png'
    },
    copies: {
      header: 'A Revolução do Bem Chegou!',
      headerPhrases: [
        {
          icon: 'fas fa-home', text: 'Campanhas e Pesquisas sobre Maquiagem'
        },
        {
          icon: 'fas fa-gift', text: 'Receba as Melhores Marcas para Testar em Casa'
        },
        {
          icon: 'fas fa-comment-dollar', text: 'Ganhe Recompensas -> a Gente Faz o Bem!'
        }
      ]
    }
  },
  'perfumes': {
    id_category: 14,
    assets: {
      header: 'produtos_tudobem_perfumes.png'
    },
    copies: {
      header: 'Participe, Ganhe e a Gente Ajuda!',
      headerPhrases: [
        {
          icon: null, text: 'Os Melhores e mais Desejados Perfumes'
        },
        {
          icon: null, text: 'Receba as Melhores Marcas para Testar em Casa'
        },
        {
          icon: null, text: 'Participe e Gahe -> Juntos a Gente Faz o Bem!'
        }
      ]
    }
  },
  'cosmeticos': {
    id_category: 1,
    assets: {
      header: 'produtos_tudobem_rosa.png'
    },
    copies: {
      header: 'Participe, Ganhe e a Gente Ajuda!',
      headerPhrases: [
        {
          icon: null, text: 'Os Melhores e mais Desejados Perfumes'
        },
        {
          icon: null, text: 'Receba as Melhores Marcas para Testar em Casa'
        },
        {
          icon: null, text: 'Ganhe por Opinar e Participar -> Juntos a Gente Faz o Bem!'
        }
      ]
    }
  },
  'bebes': {
    id_category: 23,
    assets: {
      header: 'produtos_tudobem_bebes_2.png'
    },
    copies: {
      header: 'Participe e Ganhe Produtos para Bebês!',
      headerPhrases: [
        { icon: 'fas fa-poll', text: 'Campanhas e Pesquisas sobre Fraldas e Cuidados' },
        { icon: 'fas fa-gift', text: 'Receba as Melhores Marcas para Testar em Casa' },
        { icon: 'fas fa-comment-dollar', text: 'Ganhe Recompensas -> a Gente Faz o Bem!' }
      ]
    }
  },
  'chocolates': {
    id_category: 22,
    assets: {
      header: 'produtos_tudobem_chocolates.png'
    },
    copies: {
      header: 'Ganhe Chocolates para Testar!',
      headerPhrases: [
        { icon: 'fas fa-poll', text: 'Campanhas e Pesquisas sobre Doces e Chocolates' },
        { icon: 'fas fa-gift', text: 'Receba as Melhores Marcas para Testar em Casa' },
        { icon: 'fas fa-comment-dollar', text: 'Ganhe Recompensas -> a Gente Faz o Bem!' }
      ]
    }
  },
  'barba-cabelo': {
    id_category: 41,
    assets: {
      header: 'produtos_tudobem_barba_3.png'
    },
    copies: {
      header: 'Participe e Ganhe Produtos Masculinos!',
      headerPhrases: [
        { icon: 'fas fa-poll', text: 'Campanhas e Pesquisas sobre Produtos para o Homem' },
        { icon: 'fas fa-gift', text: 'Receba as Melhores Marcas para Testar em Casa' },
        { icon: 'fas fa-comment-dollar', text: 'Ganhe Recompensas -> a Gente Faz o Bem!' }
      ]
    }
  },
  'limpeza': {
    id_category: 32,
    assets: {
      header: 'produtos_tudobem_limpeza.png'
    },
    copies: {
      header: 'Participe e Ganhe Kits de Limpeza!',
      headerPhrases: [
        { icon: 'fas fa-pump-soap', text: 'Campanhas e Pesquisas sobre Produtos de Limpeza' },
        { icon: 'fas fa-gift', text: 'Receba as Melhores Marcas para Testar em Casa' },
        { icon: 'fas fa-comment-dollar', text: 'Ganhe Recompensas -> a Gente Faz o Bem!' }
      ]
    }
  },

};

export function getCopies( category )
{
  let result = null;
  if( copies[ category ] )
    result = copies[ category ];

  return result;
}

export function executeConversionPixel( from, data )
{
  if( from === 'search' ){
    gtag('event', 'conversion', { 'send_to': 'AW-776906487/4npZCJCjw9oBEPfNuvIC', 'value': 1.0, 'currency': 'BRL' });
  }

  if( from === 'fb' )
    fbq('track', 'CompleteRegistration', { currency: 'BRL', value: 0.5 } );

  if( from === 'prop' ){

  }
}

export function openTab( url )
{
  window.open( url, '_blank' );
}
