import defaults from 'src/api/async.defaults';
import Vue from 'vue';

export async function getByUser( id )
{
  let response, r = false;
  response = await fetch( `${process.env.fetchBaseURL}userexchange/${id}`,{ method : 'GET', headers: defaults.getAuthHeaders() } );

  if( response.status === 200 )
    r = await response.json();
  else
    defaults.handleStatusError( response );

  return r;
}
