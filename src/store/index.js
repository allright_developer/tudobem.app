import Vue from 'vue'
import Vuex from 'vuex'

import guard from './guard';
import survey from './survey';
import promotion from './promotion';
// const guard = () => import('./guard');

Vue.use(Vuex);

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      guard, survey, promotion
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  });

  return Store;
}
