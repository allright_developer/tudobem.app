import social from 'src/service/social.service';
import { getSettings } from "src/service/settings.service";
import { initAnalytics } from "src/service/firebase/fcm.service";

export default async ({  app, router, Vue  }) => {

  social.init();
  Vue.prototype.$social   = social;
  Vue.prototype.$settings = await getSettings();
  Vue.prototype.$assets   = { url: 'https://tudobem.s3.sa-east-1.amazonaws.com/' };

  Vue.prototype.$analytics = initAnalytics();
}
