import { get, favorite } from 'src/service/promotion.service';

export default {

  namespaced: true,
  state:{
    promotions: null,
    promotion: null
  }

  ,mutations:
  {
    setPromotions( state, promotions )
    {
      state.promotions = promotions;
    },

    setFavorited( state, data )
    {
      state.promotions[ data.slug ].favorited = data.favorited;
    }
  }

  ,actions:
  {
    async get({commit}, user )
    {
      await get( user ).then( response => {
        commit( 'setPromotions', response ); // store result in state
      });
    },

    setFavorited({commit}, data )
    {
      commit('setFavorited', data );
    }
  }
}
