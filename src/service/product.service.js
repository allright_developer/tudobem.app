import defaults from 'src/api/async.defaults';

/**
 * Persists the user product favoritation
 * @param data
 * @returns {Promise<Response>}
 */
export async function favorite(data) {
  let response, json = null;
  response = await fetch(`${process.env.fetchBaseURL}product/favorite`, {
    headers: defaults.getAuthHeaders(),
    method: 'POST',
    body: JSON.stringify(data)
  });

  if( response.status === 200 )
    json = await response.json();
  else
    defaults.handleStatusError( response );

  return json;
}

/**
 * persists an user crypto to product exchange
 * @param data
 * @returns {Promise<*>}
 */
export async function exchange(data) {
  let response, json = null;
  response = await fetch(`${process.env.fetchBaseURL}product/exchange`, {
    headers: defaults.getAuthHeaders(),
    method: 'POST',
    body: JSON.stringify(data)
  });
  json = await response.json();

  return json;
}

/**
 * Get products by filter criteria
 * @param data
 * @param {Array} filters array of filter: [{ name, value }]
 * @returns {Promise<*>}
 */
export async function get( data, filters) {
  let response, json = null;
  let slug = '';
  if (data && data.slug)
    slug = '/' + data.slug;

  if ( filters )
    filters = '?' + filters.map( v => `${v.name}=${v.value}` ).join('&');
  else
    filters = '';
  response = await fetch(`${process.env.fetchBaseURL}product/get${slug}${filters}`, {
    method: 'GET',
    headers: defaults.getAuthHeaders()
  });

  if( response.status === 200 )
    json = await response.json();
  else
    defaults.handleStatusError( response );

  return json;
}
