export default {
  namespaced: true,
  state:{ survey: false },

  mutations:
  {
    setSurvey( state, survey )
    {
      state.survey = survey;
    }

    ,removeSurvey( state )
    {
      state.survey = false;
    }
  },

  actions:
  {
    setSurvey({commit}, survey )
    {
      commit('setSurvey', survey );
    },

    removeSurvey({commit})
    {
      commit('removeSurvey', null );
    }
  }
}
