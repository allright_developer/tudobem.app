import { getExpiredAtTimestamp, getTs } from "src/service/Helpers/timestamp.helper";
import Vue from 'vue';

export const settings = Vue.observable({ rewards: null, expiresAt: null });
/**
 * get Global settings
 * @returns {Promise<null>}
 */
export async function getSettings()
{
  if( settings.rewards === null ){
    settings.rewards = await fetchSettings();
  }
  else{
    let ts = getTs();
    if( settings.expiresAt === null || settings.expiresAt < ts ){
      settings.rewards = await fetchSettings();
      settings.expiresAt = getExpiredAtTimestamp(12, 'h' );
    }
  }

  return settings.rewards;
}

/**
 * It fetches settings from the server
 * @returns {Promise<null>}
 */
async function fetchSettings()
{
  let response, r = null;
  response = await fetch( `${process.env.fetchBaseURL}settings/public`,{ method : 'GET', headers: process.env.fetchDefaults.fetchOptions.headers } );

  if( response.status === 200 )
    r = await response.json();
  return r;
}
