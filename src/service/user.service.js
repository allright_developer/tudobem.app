import defaults from 'src/api/async.defaults';
import { secureStorage } from "src/api/crypt";

export function formatName( name )
{
  name = name.split( ' ' )[0];
  return name;
}

export async function confirm( data )
{
  let response, json = null;
  response = await fetch( `${process.env.fetchBaseURL}user/confirm`,{ headers: defaults.getAuthHeaders(), method : 'POST', body: JSON.stringify( data ) } );

  if( response.status === 200 )
    json = await response.json();
  else
    defaults.handleStatusError( response );

  return json;
}

/**
 * updates the user data in the storage
 * @param <Array> array of key:value object pairs
 */
export function setUserData( data )
{
  let yzr = secureStorage.getItem( 'yzr' );
  if( yzr )
    data.forEach( (obj, i) => {
      if( yzr.data.user[ obj.key ] )
        yzr.data.user[ obj.key ] = obj.value;
    });
  return secureStorage.setItem( 'yzr', yzr );
}

/**
 * updates the user credits in the localstorage
 * @param data
 */
export function setCredits( data )
{
  let yzr = secureStorage.getItem( 'yzr' );
  if( yzr )
    data.forEach( (obj, i) => {
      if( yzr.data.credits[ obj.key ] )
        yzr.data.credits[ obj.key ] = obj.value;
    });
  return secureStorage.setItem( 'yzr', yzr );
}
