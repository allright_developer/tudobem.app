
const routes = [
  {
    path: '/',
    component: () => import('layouts/PublicLayout.vue'),
    meta:{
      auth:false
    },
    children: [
      { path: '',label: 'Home', icon: 'fas fa-home',drawer:false, component: () => import( 'pages/public/index.vue' ), name: 'landing' },
      { path: '/como-funciona',label: 'Como Funciona', name:'como-funciona', icon:'fas fa-question',drawer:true, component: () => import('pages/public/como-funciona.vue') },
      { path: '/entrar',label: 'Login', icon:'fas fa-sign-in-alt',drawer:true, component: () => import('pages/public/entrar.vue'), name:'login' },
      { path: '/promodobem/:slug?', label: 'Promoções', icon:'fas fa-search-dollar',drawer:true, component: () => import('pages/public/promocoes.vue'), name:'promocoes' },
      { path:'ganhe-produtos-do-bem/:slug?', name: 'produtos-do-bem', label:'Produtos do Bem', 'icon':'fas fa-gifts', drawer:true, component: () => import( 'pages/loggedin/produtos' ) },
      { path:'termos-condicoes-politica-privacidade', name: 'termos', label:'', 'icon':'fas fa-file-contract', drawer:false, component: () => import( 'pages/public/termos' ) },
      { path: 'logout', name: 'logout', label: 'sair', icon:'fas fa-door-open', drawer:false, component: () => import('pages/loggedin/logout.vue') },
      { path: '/account-confirm/:token', label: '',name: 'confirmar', icon:'',drawer:false, component: () => import('pages/public/confirmar.vue') },
      { path: '/landing/:category?',label: '',drawer:false, name:'landing', icon:'fas fa-sitemap',component: () => import('pages/public/landing.vue') },
    ]

  }
  ,{
    path: '/me', label: 'Home', icon:'favorite_border',
    component: () => import('layouts/LoggedinLayout'),
    meta:{
      auth:true
    },
    children: [
      { path:'home', name: 'home',drawer:true, label: 'Início', icon: 'fas fa-home', component: () => import( 'pages/loggedin/home' ) },
      { path:'profile', name: 'profile',label:'Minha Conta','icon':'fas fa-person',drawer:false, component: () => import( 'pages/loggedin/profile' ) },
      { path: 'offerwall/:slug?', name: 'offerwall', label: 'Campanhas do Bem', icon:'fas fa-gifts',drawer:true, component: () => import('pages/loggedin/offerwall.vue') },
      { path: 'promo-do-bem/:slug?', name: 'promodobem', label: 'Promoções', icon:'fas fa-percent',drawer:true, component: () => import('pages/public/promocoes.vue') },
      // { path:'anjos-do-bem', name: 'anjos', label:'Anjos do Bem','icon':'fas fa-praying-hands',drawer:true, component: () => import( 'pages/loggedin/anjos' ) },
      { path:'produtos-do-bem/:slug?', name: 'produtos', label:'Produtos do Bem', 'icon':'fas fa-gift', drawer:true, component: () => import( 'pages/loggedin/produtos' ) },
      { path:'bemfeitores', name: 'bemfeitores', label:'Anjos BemFeitores', icon:'fas fa-heartbeat', drawer:true, component: () => import( 'pages/loggedin/bemfeitores' ) },
      { path:'voce-comenta/:slug?', name: 'comenta', label:'Você Comenta','icon':'fas fa-comments-dollar',drawer:false, component: () => import( 'pages/loggedin/voce-comenta' ) },
      { path:'ong-projetos', name: 'ong', label:'ONGs Apoiadas','icon':'fas fa-hands-helping', drawer:true, component: () => import( 'pages/loggedin/ong' ) },
      { path:'boas-vindas', name: 'welcome', label:'Nossão Missão','icon':'fas fa-heartbeat', drawer:true, component: () => import( 'pages/loggedin/boas-vindas' ) },
      { path: 'logout', name: 'userlogout', label: 'sair', icon:'fas fa-door-open', drawer:true, component: () => import('pages/loggedin/logout.vue') }
    ]
  }
];

// Always leave this as last one
if ( process.env.MODE !== 'ssr' ) {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
