
export default {

  methods:{
    email_remote(val) {
      this.form.loading = true;
      return new Promise((resolve, reject) => {

        // mocking up remote validation
        setTimeout(() => {
          let res = val !== null && val.length > 0 || '* Por favor, informe um email válido';

          // console.log('value',  res );
          this.form.loading = false;
          resolve(res);
        }, 1500);

      });
    }
    ,email_format(val) {
      let res = val && /(^$|^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$)/.test(val)
        || 'Por favor, informe um Email válido';

      if( res === true ){
        let tld = val.split('@')[1];
        let grantedTLD = /gmail.com$|hotmail.com$|hotmail.com.br$|outlook.com$|globomail.com$|yahoo.com$|yahoo.com.br$|oi.com.br$|globo.com$|bol.com.br$|uol.com.br$|live.com$|ymail.com$|facebook.com$|r7.com$|ibest.com.br$|me.com$|terra.com.br$|ig.com.br$|zipmail.com.br$|msn.com$/;
        res = grantedTLD.test( tld );
      }

      return res || 'Por favor, informe um domínio de um serviço de Email válido';
    }
  }
};

