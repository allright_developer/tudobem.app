import Vue from 'vue';
const CryptoJS = require( 'crypto-js' );
import SecureStorage from 'secure-web-storage';
const crypt = Vue.observable({
  secret: 'veD7Xr87jhA8haDf43HXUJKb43aH43gn5aX8pZcS1jX', iv: 'vnd718'
});

export const secureStorage = new SecureStorage( localStorage, {
  hash(key) {
    key = CryptoJS.SHA256(key, crypt.secret );
    return key.toString();
  },

  encrypt(data) {
    data = CryptoJS.AES.encrypt(data, crypt.secret );

    data = data.toString();

    return data;
  },
  decrypt(data) {
    data = CryptoJS.AES.decrypt(data, crypt.secret );
    data = data.toString( CryptoJS.enc.Utf8 );
    return data;
  }
});
