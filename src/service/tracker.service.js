import defaults from 'src/api/async.defaults';

/**
 * Register a click
 * @param data
 * @returns {Promise<*>}
 */
export async function click( data )
{
  let response, json = null;
  response = await fetch(`${process.env.fetchBaseURL}tracker/click`, {
    headers: defaults.getAuthHeaders(),
    method: 'POST',
    body: JSON.stringify(data)
  });

  if( response.status === 200 )
    json = await response.json();
  else
    defaults.handleStatusError( response );

  return json;
}
