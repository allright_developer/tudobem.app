import "unfetch/polyfill";
import defaults from 'src/api/async.defaults';
  /**
   * Persists the user promotion favoritation
   * @param data
   * @returns {Promise<Response>}
   */
  export async function favorite( data )
  {
    let response, json = null;
    // that's a great example of how to use async/await with fetch
    response = await fetch( `${process.env.fetchBaseURL}promotion/favorite`,{ headers: defaults.getAuthHeaders(), method : 'POST', body: JSON.stringify( data ) } );
    if( response.status === 200 )
      json = await response.json();
    else
      defaults.handleStatusError( response );

    return json;
  }

  export async function get( data )
  {
    let response, json = null;
    let slug = '';
    if( data && data.slug )
      slug = '/'+data.slug;

    if( data.auth )
      response = await fetch( `${process.env.fetchBaseURL}promotion-get`,{ headers: defaults.getAuthHeaders(), method : 'GET' } );
    else
      response = await fetch( `${process.env.fetchBaseURL}promotion${slug}`,{ method : 'GET' } );

    if( response.status === 200 )
      json = await response.json();
    else
      defaults.handleStatusError( response );

    return json.data;
  }

