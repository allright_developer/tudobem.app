// import 'unfetch/polyfill';
import defaults from 'src/api/async.defaults';
import { secureStorage } from "src/api/crypt";

export default {
  defaults : defaults.defaults,
  login,
  register,
  isAuthenticated,
  removeStorage,
  setStorage,
  addCredits,
  update,
  hasValidAuthentication,
  getTimestamp
};

async function login( data )
{
  let response, json;
  let reqOpts = {
    body: JSON.stringify( data )
  };

  response = await fetch( `${process.env.fetchBaseURL}login`,{ ...reqOpts, ...defaults.defaults.fetchOptions } );
  json     = await response.json();
  if( response.status !== 200 )
    json = { success:false, status: response.status, messages: json.messages, error : json.data.error };

  return json;
}

async function register( data )
{
  let response, json;
  let reqOpts = {
    body: JSON.stringify( data )
  };

  response = await fetch( `${process.env.fetchBaseURL}register`,{ ...reqOpts, ...defaults.defaults.fetchOptions } );
  json     = await response.json();
  if( response.status !== 200 )
      json = { success:false, status: response.status, messages: json.messages };

  return json;
}

async function addCredits( units )
{
  let response, json;
  let reqOpts = {
    body: JSON.stringify( units ), method:'POST', headers: defaults.getAuthHeaders()
  };
  response = await fetch( `${process.env.fetchBaseURL}credit`, reqOpts );
  json     = await response.json();

  if( response.status !== 200 )
    json = { success:false, status: response.status, messages: json.messages };

  return json;
}

function removeStorage()
{
  secureStorage.clear();
  localStorage.setItem( 'hasAccount', 'true' );
}

function setStorage( data )
{
  if( data ){
    data.ts = getTimestamp(); // for tests only
    if( !data.data.user_data ){
      data.data.user_data = { cep:null,cpf:'',dob: null,geo: null,address:{number:null,complement: null },children:0,economic:null, interests:[],marital_status:null,ong:null  };
    }
    secureStorage.setItem( 'yzr', data );
  }
}

function getStorage()
{
    let yzr = secureStorage.getItem( 'yzr' );
    if( yzr === null )
      yzr = { data: null, ts: 0 };

    return yzr;
}

function getTimestamp()
{
  return Math.floor(Date.now() / 1000) + (15*24*60*60); // 15 days to renew token
}

function getCurrentTimestamp()
{
  return Math.floor(Date.now() / 1000);
}

function isAuthenticated()
{
  let isAuthenticated = false;
  let yzr = null;
  try{
      yzr = secureStorage.getItem( 'yzr' );
  } catch( e ){
      return isAuthenticated;
  }

  if( yzr ){
    isAuthenticated = yzr.data.token && yzr.data.user;
  }
  return isAuthenticated ? yzr : false;
}

function hasValidAuthentication()
{
  let isAuth = true;
  let data = getStorage();
  let ts = getCurrentTimestamp();
  if( data.ts < ts )
    isAuth = false;

  return isAuth;
}

async function update( data )
{
  let response, json = null;
  response = await fetch( `${process.env.fetchBaseURL}user/update`,{ headers: defaults.getAuthHeaders(), method : 'POST', body: JSON.stringify( data ) } );
  json     = await response.json();

  if( response.status === 200 ){ // updates storage
    let yzr = secureStorage.getItem( 'yzr' );
    yzr.data.user.gender = data.gender;
    yzr.data.user_data = {
      economic: data.economic, marital_status: data.marital_status, interests: data.interests, children: data.children,
      dob: data.dob, geo: data.geo, cpf: data.cpf, cep: data.cep, address: data.address
    };
    secureStorage.setItem( 'yzr', yzr );
  }
  return json;
}

