import defaults from 'src/api/async.defaults';

export async function get( data )
{
  let response, json = null;
  let now = Math.floor(Date.now() / 1000);
  json = JSON.parse( localStorage.getItem('angel_list' ) );
  if( json === null || (json && json.timestamp < now) ){

    response = await fetch( `${process.env.fetchBaseURL}get-angels`,{ headers: defaults.getAuthHeaders(), method : 'GET' } );
    if( response.status === 200 ){
      json = await response.json();
      localStorage.setItem( 'angel_list', JSON.stringify( { data: json, timestamp: now + (2*60) } ) );
    }
    else if( response.status >= 400 && response.status <= 500 ){
      defaults.handleStatusError( response );
    }
    else
      json = null;
  }
  else
    json = json.data;

  return json;
}

export async function getGifts( data )
{
  let response, json = null;
  let now = Math.floor(Date.now() / 1000);
  json = JSON.parse( localStorage.getItem('angel_gifts' ) );
  if( json === null || (json && json.timestamp < now) ){

    response = await fetch( `${process.env.fetchBaseURL}angel/gifts`,{ headers: { 'Content-type': 'application/json' }, method : 'GET' } );

    if( response.status === 200 ) {
      json = await response.json();
      localStorage.setItem( 'angel_gifts', JSON.stringify( { data: json, timestamp: now + (0*60) } ) );
    }
    else
      json = null;
  }
  else
    json = json.data;

  return json;
}

