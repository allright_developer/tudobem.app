export function cep( cep )
{
  let r = true;

  if( cep.length < 9 )
    r = 'Por favor, corrija o seu CEP';

  return r;
}

function calcChecker1 (firstNineDigits) {
  let sum = null;

  for (let j = 0; j < 9; ++j) {
    sum += firstNineDigits.toString().charAt(j) * (10 - j)
  }

  const lastSumChecker1 = sum % 11
  const checker1 = (lastSumChecker1 < 2) ? 0 : 11 - lastSumChecker1;

  return checker1;
}

function calcChecker2 (cpfWithChecker1) {
  let sum = null;

  for (let k = 0; k < 10; ++k) {
    sum += cpfWithChecker1.toString().charAt(k) * (11 - k)
  }
  const lastSumChecker2 = sum % 11
  return ( (lastSumChecker2 < 2) ? 0 : 11 - lastSumChecker2 );

}

function cleaner (value) {
  return value.replace(/[^\d]/g, '');
}

function cpfValidateMod ( value ) {
  const cleanCPF = cleaner( value );
  const firstNineDigits = cleanCPF.substring(0, 9);
  const checker = cleanCPF.substring(9, 11);
  let result = false;

  for (let i = 0; i < 10; i++) {
    if ( firstNineDigits + checker === Array(12).join(i) ) {
      return false;
    }
  }

  const checker1 = calcChecker1(firstNineDigits);
  const checker2 = calcChecker2(`${firstNineDigits}${checker1}`);

  result = checker.toString() === checker1.toString() + checker2.toString();

  return result;
}

/**
 * @param {String} val
 * @returns {boolean | string}
 */
function isValidEmail ( val ) {
  const emailPattern = /^(?=[a-zA-Z0-9@._%+-]{6,254}$)[a-zA-Z0-9._%+-]{1,64}@(?:[a-zA-Z0-9-]{1,63}\.){1,8}[a-zA-Z]{2,63}$/;
  return emailPattern.test( val ) || 'Invalid email';
}
/**
 * @param {String} cpf
 * @returns {boolean}
 */
export function cpf( cpf ){

  let r = true;

  if( cpf ){
    if( cpf.length < 14 )
      r = 'Por favor, verifique se o seu CPF está correto';

    if( cpf.length === 14 ){
      if( cpfValidateMod( cpf ) === false )
        r = 'O CPF informado é inválido.';
    }
  }

  return r;
}

export function notEmpty( val ) {
  let r = true;

  if( typeof val === 'string' ){
    r = val.length === 0 ? 'Por favor, preencha este campo para prosseguir' : r;
  }

  return r;
}

export function email( val ){
  let r = true;

  r = val.length < 5 ? 'Por favor, detalhe o seu Email' : r;

  if( r === true )
    r = isValidEmail( val );

  return r;
}
