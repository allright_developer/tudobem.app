import defaults from 'src/api/async.defaults';
import Vue from 'vue';

export const state = Vue.observable({ categories: [], hierarchycallyCategories: [] });

export const getters = {
  getCategories: () => state.categories,
  hierarchicallyCategories: () => state.hierarchycallyCategories
}

export const mutations = {
  setCategories: ( categories ) => state.categories = categories,
  setHierarchyCategories: ( categories ) => state.hierarchycallyCategories = categories
};

export const actions = {
  async getHierarchically()
  {
    let response, json = null;
    if( state.hierarchycallyCategories.length === 0 ){
      response = await fetch(`${process.env.fetchBaseURL}category/hierarchically`,{ method : 'GET' } );
      json = await response.json();
      mutations.setHierarchyCategories( json );
    }
    return json;
  },

  async fetchCategories( formatToSelect )
  {
    let response, json = null;
    if( state.categories.length === 0 ){

      response = await fetch( `${process.env.fetchBaseURL}category`,{ method : 'GET' } );
      json     = await response.json();

      if( formatToSelect ) {
        let options = [];
        for( let i in json.options ){
          options.push( { value: json.options[i].value, label: json.options[i].label, desc: json.options[i].description } );
        }
        mutations.setCategories( options );
      }
      else
        mutations.setCategories( json.options );
    }
  }

};
