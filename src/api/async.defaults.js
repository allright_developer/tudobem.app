import { secureStorage } from "src/api/crypt";

export default {
  defaults: {
    fetchOptions:{
      method:'POST',
      headers: {
        'Content-type':'application/json'
      }
    }
  }
  ,getAuthHeaders() {
    let yzr = secureStorage.getItem( 'yzr' );
    let headers = { 'Content-type': 'application/json' };
    if( yzr ){
      headers[ 'Authorization' ] = `Bearer ${yzr.data.token}`;
    }

    return headers;
  }

  ,objectToQuery( obj )
  {
    let r = '';
    if( obj )
      r = '?'+Object.keys( obj ).map( key => obj[ key ] ? key + '=' + obj[ key ] : '' ).join( '&' );
    return r;
  }
  ,handleStatusError( response )
  {
    let res = { success: true, message: 'ok' };

    if( response.status === 401 ){
      secureStorage.clear();
      window.location.href = '/entrar';
      res = { success: false, message: 'Unauthorized' };
    }

    if( response.status === 421 ){
      res = { success: false, message: 'Application Error' };
    }

    if( response.status >= 500 )
      res = { success: false, message: 'Server Error' };

    return res;
  }
};
