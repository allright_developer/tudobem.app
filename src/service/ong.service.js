import defaults from 'src/api/async.defaults';
import Vue from 'vue';
import { secureStorage } from "src/api/crypt";

export const state = Vue.observable({ ongs: [], categories: [] });

export const getters = {
  getCategories: () => state.categories,
  getOngs: () => state.ongs
}

export const mutations = {
  setCategories: ( categories ) => state.categories = categories,
  setOngs: ( ongs ) => state.ongs = ongs,
  setFavorite( i ){
    state.ongs.forEach( ong => ong.favorited = false );
    state.ongs[i].favorited = true;
    let d = secureStorage.getItem('yzr');
    d.data.user_data.ong = state.ongs[i];
    secureStorage.setItem( 'yzr', d );
  }
};

export async function get(id)
{
  id = id || 0;
  let response, r = false;
  response = await fetch( `${process.env.fetchBaseURL}ong/${id}`,{ method : 'GET', headers: defaults.getAuthHeaders() } );

  if( response.status === 200 )
    r = await response.json();
  else
    defaults.handleStatusError( response );

  return r;
}

export const actions = {
  async fetchOngs()
  {
    let response, json = null;
    if( state.ongs.length === 0 ){
      response = await fetch(`${process.env.fetchBaseURL}ong`,{ method : 'GET' } );
      json = await response.json();
      let favorited = parseInt( secureStorage.getItem( 'yzr' ).data.user_data.ong );
      json.forEach( ong => {
        if( favorited )
          ong.favorited = ong.id === favorited;
        else
          ong.favorited = false;
        ong.loading   = false;
      })
      mutations.setOngs( json );
    }
  },

  async fetchCategories( formatToSelect )
  {
    let response, json = null;
    if( state.categories.length === 0 ){

      response = await fetch( `${process.env.fetchBaseURL}category`,{ method : 'GET' } );
      json     = await response.json();

      if( formatToSelect ) {
        let options = [];
        for( let i in json.options ){
          options.push( { value: json.options[i].value, label: json.options[i].label } );
        }
        mutations.setCategories( options );
      }
      else
        mutations.setCategories( json.options );
    }
  },

  async setFavorite(i)
  {
    let response, r = false;
    state.ongs[i].loading = true;
    response = await fetch( `${process.env.fetchBaseURL}ong/favorite/${state.ongs[i].id}`,{ method : 'POST', headers: defaults.getAuthHeaders() } );

    if( response.status === 200 ){
      mutations.setFavorite(i);
      r = true;
      this.$q.notify({ type: 'positive', message: state.ongs[i].label+' agora é sua ONG Favorita.', timeout: 4000, position: 'center' } );
      this.$analytics.logEvent( 'select_content', { content_type: `ong-favorite`, content_id: state.ongs[i].id});
    }
    else
      defaults.handleStatusError( response );

    state.ongs[i].loading = false;
    return r;
  }

};
