import defaults from 'src/api/async.defaults';

/**
 * Send data after an user Accept Action
 * @param data
 * @returns {Promise<Response>}
 */
export async function post(data) {
  let response, json = null;
  response = await fetch(`${process.env.fetchBaseURL}offerwall`, {
    headers: defaults.getAuthHeaders(),
    method: 'POST',
    body: JSON.stringify(data)
  });
  json = await response.json();
  return json;
}

/**
 * get the offers
 * @param {Object} data
 * @returns {Promise<Object>}
 */
export async function get(data) {
  let response, json = null;
  data = defaults.objectToQuery(data);
  response = await fetch(`${process.env.fetchBaseURL}offerwall${data}`, {
    headers: defaults.getAuthHeaders(),
    method: 'GET'
  });
  json = await response.json();

  if (json.data.offers)
    json.data.offers.forEach((offer, i) => {
      offer.conf = JSON.parse(offer.conf);
      offer.text = JSON.parse(offer.text);
    });

  return json.data;
}

export async function getById( id )
{
  id = id || '';
  let response, json = null;
  response = await fetch(`${process.env.fetchBaseURL}offerwall/get-by-id/${id}`, {
    headers: defaults.getAuthHeaders(),
    method: 'GET'
  });
  json = await response.json();

  if ( json.offer ){
    json.offer.conf = JSON.parse( json.offer.conf );
    json.offer.text = JSON.parse( json.offer.text );
  }
  return json.offer;
}

/**
 * Count up a new Offer Impression
 * @param {Object} data
 * @returns {Promise<Object>}
 */
export async function impression(data) {
  let response, json = null;
  response = await fetch(`${process.env.fetchBaseURL}offerwall/offer/impression`, {
    headers: defaults.getAuthHeaders(),
    method: 'POST',
    body: JSON.stringify(data)
  });

  if( response.status === 200 )
    json = await response.json();
  else
    defaults.handleStatusError( response );

  return json;
}

/**
 *
 * @param {Object} data keys: offerwall hash token
 * @returns {Promise<*>}
 */
export async function finish(data) {
  let response, json = null;
  response = await fetch(`${process.env.fetchBaseURL}offerwall/finish-impression`, {
    headers: defaults.getAuthHeaders(),
    method: 'POST',
    body: JSON.stringify(data)
  });

  if( response.status === 200 )
    json = await response.json();
  else
    defaults.handleStatusError( response );

  return json;
}
