import defaults from 'src/api/async.defaults';

export default
{
    headers:null,
    metadata:{
      attr:{
        'date':{ mask: '##/##/####', placeholder: 'Formato: DD/MM/AAAA' },
        'datetime':{ mask: '##/##/#### ##:##', placeholder:'Formato: DD/MM/AAAA hh:mm' }
      }
      ,messages:[
        '<p class="notify text-secondary"><b>[name]</b>, sua Opinião foi muito Importante!</p>' +
        '<p class="notify">Você Ganhou <b>[crypto] Credibens</b> e outros <b>[crypto_org] Credibens</b> foram reservados para Doação</p>' +
        '<p class="notify">Obrigado <i class="fas fa-heart fa-2x text-secondary"></i></p>',

        `<p class="notify text-secondary">Obrigado por Responder à Pesquisa <b>[name]</b>!</p><p>Você ganhou <b class="positive">[crypto] Credibens</b> e
            reservamos <b class="text-blue-5">[crypto_org] Credibens</b> para Doação. <i class="fas fa-heart fa-2x text-secondary"></i></p>`,

        `<p class="notify">Sua Opinião acaba de ser recompensada, <b>[name]</b>!</p>
         <p class="notify"><b>[crypto] Credibens</b> para você e mais <b>[crypto_org] Credibens</b> vamos doar e Fazer o Bem  <i class="fas fa-heart fa-2x text-secondary"></i></p>`
      ]
    },

    async persist( survey )
    {
      let response, json = null;
      // that's a great example of how to use async/await
      response = await fetch( `${process.env.fetchBaseURL}answer`,{ headers: defaults.getAuthHeaders(), method : 'POST', body: JSON.stringify( survey ) } );
      json     = await response.json();

      if( response.status === 200 ){
        response = json.data;
      }
      else
        defaults.handleStatusError( response );

      return response;
    }
    /**
     * @param {String} surveySlug
     * @returns {{questions: ({val: null, answers: [{val: string, color: string, label: string, type: string}]})[], title: string}}
     */
    ,async get( surveySlug )
    {
      let response, json = null;

      if( surveySlug ){
        response = await fetch( `${process.env.fetchBaseURL}survey/get/${surveySlug}`,{ headers: defaults.getAuthHeaders() } );
        json     = await response.json();

        if( response.status === 200 ){
          json = json.data;
        }
        else
          defaults.handleStatusError( response );
      }

      return json;
    }

    ,getThankYouMessage( data )
    {
      let msgIdx = Math.floor(Math.random() * this.metadata.messages.length );
      let message = this.metadata.messages[ msgIdx ];
      Object.keys( data ).forEach( ( field, value ) => {
        message = message.replace(`[${field}]`, data[field]);
      });

      message = message.replace('[name]', data.name );
      return message;
    }
};
