import ApiService from '../service/api.mock.service';

let initialState = { authenticated: false, user: null };
try{
  const user = JSON.parse( localStorage.getItem('yzr') );
  initialState = user ?
    { authenticated: true, user: user } :
    { authenticated: false, user: null };
}
catch( e ){
}

export default {
  namespaced: true,
  state: initialState,

  mutations:{

    login( state, data ){
      if( data ){

      }
    }
    ,logout( state ){

    }
    ,register( state, data ){

      // if( data )
      //   localStorage.setItem( 'yzr', JSON.stringify( data ) );
    }
  },

  actions:
  {
    login({ commit }, { email, passwd }){
       ApiService.login({email, passwd}).then( json => {
          commit( 'login', data );
       })
      .catch( error => {
        console.error('login error', error );
      });
    }

    ,register({commit}, {name, email} ){
        ApiService.register({name, email} ).then( json => {

          if( json !== null ){
            commit( 'register', json );
            commit( 'login', json );
          }
        });
    }
  }

}
