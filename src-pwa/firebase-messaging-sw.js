importScripts('https://www.gstatic.com/firebasejs/7.19.1/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/7.19.1/firebase-messaging.js')

firebase.initializeApp({
  apiKey: "AIzaSyDAZYJ6H7-U3ccNF2gdweOfqyD90hueWYI",
  authDomain: "tudobem.firebaseapp.com",
  databaseURL: "https://tudobem.firebaseio.com",
  projectId: "tudobem",
  storageBucket: "tudobem.appspot.com",
  messagingSenderId: "550152183565",
  appId: "1:550152183565:web:924e3a5d9f5584baf41a44",
  measurementId: "G-T6L0CYDGP1"
});

const messaging = firebase.messaging();
// background notifications listener
messaging.setBackgroundMessageHandler( async payload => {
  // Customize notification here
  const notificationTitle = 'Título da Mensagem';
  const notificationOptions = {
    body: 'Mensagem a ser aplicada no body do dialog',
    icon: 'icons/apple-icon-120x120.png',
    badge: 'icons/apple-icon-120x120.png',
    image: 'img/tudobem-logo-fonte-512.png'
  };

  return self.registration.showNotification( notificationTitle, notificationOptions );
});

