import * as firebase from 'firebase/app';
import 'firebase/messaging';
import 'firebase/analytics';
import Vue from 'vue';
import defaults from 'src/api/async.defaults';

const firebaseConfig = {
  apiKey: "AIzaSyDAZYJ6H7-U3ccNF2gdweOfqyD90hueWYI",
  authDomain: "tudobem.firebaseapp.com",
  databaseURL: "https://tudobem.firebaseio.com",
  projectId: "tudobem",
  storageBucket: "tudobem.appspot.com",
  messagingSenderId: "550152183565",
  appId: "1:550152183565:web:924e3a5d9f5584baf41a44",
  measurementId: "G-T6L0CYDGP1"
};

export const state = Vue.observable({firebase: null, messaging: null, analytics: null, registration: null});

/**
 * inits the firebase object
 * @returns {firebase}
 */
export function initApp() {
  state.firebase = firebase;
  state.firebase.initializeApp(firebaseConfig);
  return state.firebase;
}

/**
 * @returns {firebase.analytics.Analytics}
 */
export function initAnalytics() {
  if (state.firebase === null)
    initApp();

  state.analytics = firebase.analytics();

  return state.analytics;
}

/**
 *
 * @returns {firebase.messaging.Messaging}
 */
export function initMessaging() {
  if (state.firebase === null)
    initApp();

  state.messaging = firebase.messaging();
  state.messaging.usePublicVapidKey('BFPLyfHCVBuVMrchUQbJZcmRN37BaDH1zKrLXDyFXn1FXy-KSumY6yJrYxtlTjcrd1P7YuvXN0sJZk4rkUKH3I4');
}

export function requestPushPermission() {
  let pushStatus = getPermissionStatus();
  if( pushStatus.ask ) {

    Notification.requestPermission().then( permission => {
      if ( permission === 'granted' ) {
        state.messaging.getToken().then(token => {
          subscribeToTopic( token, 'institutional' ).then( res => {}); // subscribe the user to institutional topic
        })
        .catch(error => console.log('error on get token', error));

        // initBackgroundMessageHandler( state.registration );
      }
    }).catch( error => console.log('error on permission ', error) );
  }

}

/**
 * Init the background push message notification listener
 * @param {ServiceWorkerRegistration} registration
 */
export function initBackgroundMessageHandler(registration) {
  if (state.firebase === null)
    initMessaging();

  state.messaging.setBackgroundMessageHandler(async payload => {
    // Customize notification here
    console.log('BACKGROUND MESSAGE PAYLOAD', payload);
    const notificationTitle = 'Título da Mensagem';
    const notificationOptions = {
      body: 'Mensagem a ser aplicada no body do dialog',
      icon: 'icons/apple-icon-120x120.png',
      badge: 'icons/apple-icon-120x120.png',
      image: 'img/tudobem-logo-fonte-512.png'
    };
    return registration.showNotification(notificationTitle, notificationOptions);
  });
}

export async function subscribeToTopic( token, topic )
{
  topic = topic || 'institutional';
  /* // an example of how to subscribe an user directly to firebase messaging
  let endpoint = `https://iid.googleapis.com/iid/v1/${token}/rel/topics/${topic}`;
  let headers = {
    'Content-Type' : 'application/json',
    'Authorization': `key=AAAAgBegHw0:APA91bHhAaxGHe5Fxsq57R5VNQXaccp0dwWB6Q9JD1ZDY95xOpM7xRszzQf8u-8by5yDxLScBhXjy9TOWTKipLZcqzSFOx00DnLQQv9kSLhyjK6G_-LuSrziMaxhco-8f2s3oQwUdRIC`
  }*/

  let response = await fetch( `${process.env.fetchBaseURL}push/subscribe/${token}`,
    { method : 'POST', headers: defaults.getAuthHeaders(), body: JSON.stringify( { topic } )
  });
  return await response.json();
}
/**
 * test method
 * @param data
 * @returns {Promise<void>}
 */
export async function sendMessage(data) {
  fetch(`https://fcm.googleapis.com/v1/projects/tudobem/messages:send`,
    {
      method: 'POST',
      headers: {
        'Authorization': `Bearer AAAAgBegHw0:APA91bHhAaxGHe5Fxsq57R5VNQXaccp0dwWB6Q9JD1ZDY95xOpM7xRszzQf8u-8by5yDxLScBhXjy9TOWTKipLZcqzSFOx00DnLQQv9kSLhyjK6G_-LuSrziMaxhco-8f2s3oQwUdRIC`,
        'Content-type': 'application/json',
        body: JSON.stringify(
          {
            "message": {
              "token": "cZroBZ-6lhv_bCAFuFXoGW:APA91bGAo4oMJlCLjCgewjb1SdQm6kjOkbUIFMyAPypBE9oXNh8Utc49HNu-kn0gq3awqjF9VxSKNyho38x7k9FxJXUNX6y8kCWz8cZGDqSsUBi9Nx2JTprLrlBP7VZmUXVsa-knBCuY",
              "webpush": {
                "headers": {
                  "Urgency": "high"
                },
                "notification": {
                  "body": "Uma mensagem Firestore Tudobem",
                  "requireInteraction": "true",
                  "badge": "/icons/apple-icon-120x120.png"
                }
              }
            }
          }
        )
      }
    }).then(response => {
    response.json().then(json => console.log('json returned', json));
  });
}

export function getPermissionStatus()
{
  let status = { ask: false, message: 'Notificações Desativadas' };
  if( 'Notification' in window ){
    if( Notification.permission === 'default' ){
      status = { ask: true, message: 'Ativar Notificações' };
    }

    if( Notification.permission === 'granted' ){
      status = { ask: false, message: 'Notificações Ativadas' };
    }
  }

  return status;
}
