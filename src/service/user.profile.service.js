import defaults from 'src/api/async.defaults';

export async function update( data )
{
  let response, json = null;
  response = await fetch( `${process.env.fetchBaseURL}user/profile/update`,{ headers: defaults.getAuthHeaders(), method : 'POST' } );
  if( response.status === 200 )
    json = await response.json();
  else
    defaults.handleStatusError( response );

  return json;
}
export async function get( data )
{
  let response, json = null;
  response = await fetch( `${process.env.fetchBaseURL}user/profile/get`,{ headers: defaults.getAuthHeaders(), method : 'GET' } );
  if( response.status === 200 )
    json = await response.json();
  else
    defaults.handleStatusError( response );

  return JSON.parse( json );
}
